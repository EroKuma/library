package org.example.es;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.DateFormat;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.time.LocalDateTime;

@Document(indexName = "rental-index")
public class RentalEntity {
    @Id
    private String rentalId;
    @Field(type = FieldType.Keyword)
    private String bookId;
    @Field(type = FieldType.Keyword)
    private String customerId;
    @Field(type = FieldType.Integer)
    private int days;
    @Field(type = FieldType.Boolean)
    private boolean isReturned = false;
    @Field(type = FieldType.Boolean)
    private boolean isReturnedLate = false;
    @Field(type = FieldType.Date, format = DateFormat.basic_date_time_no_millis)
    private LocalDateTime rentDate;
    @Field(type = FieldType.Date, format = DateFormat.basic_date_time_no_millis)
    private LocalDateTime dueDate;
    @Field(type = FieldType.Date, format = DateFormat.basic_date_time_no_millis)
    private LocalDateTime returnDate;
    @Field(type = FieldType.Double)
    private double charge;
    @Field(type = FieldType.Double)
    private double fines;

    public String getRentalId() { return rentalId; }
    public void setRentalId(String rentalId) { this.rentalId = "R"+rentalId; }

    public String getBookId() { return bookId; }
    public void setBookId(String bookId) { this.bookId = bookId; }

    public String getCustomerId() { return customerId; }
    public void setCustomerId(String customerId) { this.customerId = customerId; }

    public int getDays() { return days; }
    public void setDays(int days) { this.days = days; }

    public boolean getIsReturned() { return isReturned; }
    public void setIsReturned(boolean isReturned) { this.isReturned = isReturned; }

    public boolean getIsReturnedLate() { return isReturnedLate; }
    public void setIsReturnedLate(boolean isReturnedLate) { this.isReturnedLate = isReturnedLate; }

    public LocalDateTime getRentDate() { return rentDate; }
    public void setRentDate(LocalDateTime rentDate) { this.rentDate = rentDate; }

    public LocalDateTime getDueDate() { return dueDate; }
    public void setDueDate(LocalDateTime dueDate) { this.dueDate = dueDate; }

    public LocalDateTime getReturnDate() { return returnDate; }
    public void setReturnDate(LocalDateTime returnDate) { this.returnDate = returnDate; }

    public double getCharge() { return charge; }
    public void setCharge(double charge) { this.charge = charge; }

    public double getFines() { return fines; }
    public void setFines(double fines) { this.fines = fines; }

    public double chargeCal(int days) {return 4*days;}
    public double chargeMemberCal(int days) {return 4*days - (4*days*0.1);}

    public double finesCal(int days) {return 5*days;}
}
