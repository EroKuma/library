package org.example.es;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Document(indexName = "book-index")
public class BookEntity {
    @Id
    private String bookId;
    @Field(type = FieldType.Keyword)
    private String title;
    @Field(type = FieldType.Keyword)
    private String author;
    @Field(type = FieldType.Double)
    private double price;
    @Field(type = FieldType.Boolean)
    private boolean isAvailable;

    public String getBookId() { return bookId; }
    public void setBookId(String bookId) { this.bookId = "B"+bookId; }

    public String getTitle() { return title; }
    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() { return author; }
    public void setAuthor(String author) {
        this.author = author;
    }

    public double getPrice() { return price; }
    public void setPrice(double price) { this.price = price; }

    public boolean getIsAvailable() {return isAvailable;}
    public void setIsAvailable(boolean isAvailable) {this.isAvailable = isAvailable;}
}
