package org.example.es;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Document(indexName = "customer-index")
public class CustomerEntity {
    @Id
    private String customerId;
    @Field(type = FieldType.Keyword)
    private String firstName;
    @Field(type = FieldType.Keyword)
    private String lastName;
    @Field(type = FieldType.Keyword)
    private String tel;
    @Field(type = FieldType.Boolean)
    private boolean isMember;

    public String getCustomerId() { return customerId; }
    public void setCustomerId(String customerId) { this.customerId = "C"+customerId; }

    public String getFirstName() { return firstName; }
    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getLastName() { return lastName; }
    public void setLastName(String lastName) { this.lastName = lastName; }

    public String getTel() { return tel; }
    public void setTel(String tel) { this.tel = tel; }

    public boolean getIsMember() { return isMember; }
    public void setIsMember(boolean isMember) { this.isMember = isMember; }
}
