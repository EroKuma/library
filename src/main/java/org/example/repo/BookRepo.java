package org.example.repo;

import org.example.es.BookEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepo extends ElasticsearchRepository<BookEntity,String> {
    List<BookEntity> findAll();
    List<BookEntity> findByTitle(String title);
    List<BookEntity> findByAuthor(String author);
    List<BookEntity> findByIsAvailable(boolean isAvailable);
}
