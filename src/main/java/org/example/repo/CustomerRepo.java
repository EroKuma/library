package org.example.repo;

import org.example.es.CustomerEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerRepo extends ElasticsearchRepository<CustomerEntity,String> {
    List<CustomerEntity> findAll();
    List<CustomerEntity> findCustomerByFirstName(String firstName);
    List<CustomerEntity> findCustomerByLastName(String lastName);
    List<CustomerEntity> findCustomerByIsMember(boolean isMember);
}
