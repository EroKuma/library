package org.example.repo;

import org.example.es.RentalEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RentalRepo extends ElasticsearchRepository<RentalEntity,String> {
    List<RentalEntity> findAll();
    List<RentalEntity> findByBookId(String bookId);
    List<RentalEntity> findByCustomerId(String customerId);
    List<RentalEntity> findByIsReturned(boolean isReturned);
    List<RentalEntity> findByIsReturnedLate(boolean isReturnedLate);
}
