package org.example.service;

import org.example.es.BookEntity;
import org.example.es.CustomerEntity;
import org.example.es.RentalEntity;
import org.example.repo.BookRepo;
import org.example.repo.CustomerRepo;
import org.example.repo.RentalRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Service
public class LibraryService {
    private int bookNum;
    private int customerNum;
    private int rentalNum;
    @Autowired
    private BookRepo bookRepo;
    @Autowired
    private CustomerRepo customerRepo;
    @Autowired
    private RentalRepo rentalRepo;

    @Autowired
    private void init(){
        bookRepo.deleteAll();

        List<BookEntity> bookEntities = new ArrayList<>();

        BookEntity book = new BookEntity();
        bookNum++;
        book.setBookId(String.valueOf(bookNum));
        book.setTitle("Hyouka");
        book.setAuthor("Honobu Yonezawa");
        book.setPrice(200.00);
        book.setIsAvailable(true);
        bookEntities.add(book);

        book = new BookEntity();
        bookNum++;
        book.setBookId(String.valueOf(bookNum));
        book.setTitle("Death Note");
        book.setAuthor("Tsugumi Ohba");
        book.setPrice(999.99);
        book.setIsAvailable(false);
        bookEntities.add(book);

        book = new BookEntity();
        bookNum++;
        book.setBookId(String.valueOf(bookNum));
        book.setTitle("Bahamut");
        book.setAuthor("Senri Akatsuki");
        book.setPrice(250.55);
        book.setIsAvailable(false);
        bookEntities.add(book);

        bookRepo.saveAll(bookEntities);

        //

        customerRepo.deleteAll();

        List<CustomerEntity> customerEntities = new ArrayList<>();

        CustomerEntity customer = new CustomerEntity();
        customerNum++;
        customer.setCustomerId(String.valueOf(customerNum));
        customer.setFirstName("Nopparuj");
        customer.setLastName("Bangphrathaiya");
        customer.setTel("0915197672");
        customer.setIsMember(true);
        customerEntities.add(customer);

        customer = new CustomerEntity();
        customerNum++;
        customer.setCustomerId(String.valueOf(customerNum));
        customer.setFirstName("Kanna");
        customer.setLastName("Makkana");
        customer.setTel("0917245687");
        customer.setIsMember(false);
        customerEntities.add(customer);

        customerRepo.saveAll(customerEntities);

        //

        rentalRepo.deleteAll();

        List<RentalEntity> rentalEntities = new ArrayList<>();

        RentalEntity rental = new RentalEntity();
        rentalNum++;
        rental.setRentalId(String.valueOf(rentalNum));
        rental.setBookId("B1");
        rental.setCustomerId("C1");
        rental.setDays(7);
        rental.setIsReturned(true);
        rental.setIsReturnedLate(false);
        rental.setRentDate(LocalDateTime.now().minusDays(14));
        rental.setDueDate(rental.getRentDate().plusDays(rental.getDays()));
        rental.setReturnDate(rental.getRentDate().plusDays(5));
        rental.setCharge(rental.chargeMemberCal(rental.getDays()));

        rentalEntities.add(rental);

        rental = new RentalEntity();
        rentalNum++;
        rental.setRentalId(String.valueOf(rentalNum));
        rental.setBookId("B2");
        rental.setCustomerId("C2");
        rental.setDays(7);
        rental.setIsReturned(false);
        rental.setIsReturnedLate(false);
        rental.setRentDate(LocalDateTime.now().minusDays(10));
        rental.setDueDate(rental.getRentDate().plusDays(rental.getDays()));
        rental.setCharge(rental.chargeCal(rental.getDays()));
        rentalEntities.add(rental);

        rental = new RentalEntity();
        rentalNum++;
        rental.setRentalId(String.valueOf(rentalNum));
        rental.setBookId("B3");
        rental.setCustomerId("C2");
        rental.setDays(7);
        rental.setIsReturned(false);
        rental.setIsReturnedLate(false);
        rental.setRentDate(LocalDateTime.now());
        rental.setDueDate(rental.getRentDate().plusDays(rental.getDays()));
        rental.setCharge(rental.chargeCal(rental.getDays()));
        rentalEntities.add(rental);

        rentalRepo.saveAll(rentalEntities);
    }

    public List<BookEntity> getAllBook() {return bookRepo.findAll();}
    public BookEntity getBookById(String bookId) {return bookRepo.findById(bookId).get();}
    public List<BookEntity> getBookByTitle(String title) {return bookRepo.findByTitle(title);}
    public List<BookEntity> getBookByAuthor(String author) {return bookRepo.findByAuthor(author);}
    public List<BookEntity> getBookByIsAvailable(boolean isAvailable) {return bookRepo.findByIsAvailable(isAvailable);}
    public BookEntity addBook(String title,String author,double price){
        BookEntity book = new BookEntity();
        bookNum++;
        book.setBookId(String.valueOf(bookNum));
        book.setTitle(title);
        book.setAuthor(author);
        book.setPrice(price);
        book.setIsAvailable(true);
        return bookRepo.save(book);
    }
    public void deleteBook(String bookId) { bookRepo.deleteById(bookId); }

    //

    public List<CustomerEntity> getAllCustomer() {return customerRepo.findAll();}
    public CustomerEntity getCustomerById(String customerId) {return customerRepo.findById(customerId).get();}
    public List<CustomerEntity> getCustomerByFirstName(String firstName) {return customerRepo.findCustomerByFirstName(firstName);}
    public List<CustomerEntity> getCustomerByLastName(String lastName) {return customerRepo.findCustomerByLastName(lastName);}
    public List<CustomerEntity> getCustomerByIsMember(boolean isMember) {return customerRepo.findCustomerByIsMember(isMember);}
    public CustomerEntity addCustomer(String firstName,String lastName,String tel,boolean isMember){
        CustomerEntity customer = new CustomerEntity();
        customerNum++;
        customer.setCustomerId(String.valueOf(customerNum));
        customer.setFirstName(firstName);
        customer.setLastName(lastName);
        customer.setTel(tel);
        customer.setIsMember(isMember);
        return customerRepo.save(customer);
    }
    public void deleteCustomer(String customerId) {customerRepo.deleteById(customerId);}

    //

    public List<RentalEntity> getAllRental() {return rentalRepo.findAll();}
    public RentalEntity getRentalById(String rentalId) {return rentalRepo.findById(rentalId).get();}
    public List<RentalEntity> getRentalByBookId(String bookId) {return rentalRepo.findByBookId(bookId);}
    public List<RentalEntity> getRentalByCustomerId(String customerId) {return rentalRepo.findByCustomerId(customerId);}
    public List<RentalEntity> getRentalByIsReturned(boolean isReturned) {return rentalRepo.findByIsReturned(isReturned);}
    public List<RentalEntity> getRentalByIsReturnedLate(boolean isReturnedLate) {return rentalRepo.findByIsReturnedLate(isReturnedLate);}
    public RentalEntity addRental(String bookId, String customerId, int days){

        BookEntity book = getBookById(bookId);
        CustomerEntity customer = getCustomerById(customerId);

        if (!book.getIsAvailable()){
            return null;
        }else {
            book.setIsAvailable(false);
            bookRepo.save(book);

            RentalEntity rental = new RentalEntity();
            rentalNum++;
            rental.setRentalId(String.valueOf(rentalNum));
            rental.setBookId(bookId);
            rental.setCustomerId(customerId);
            rental.setDays(days);
            rental.setRentDate(LocalDateTime.now());
            rental.setDueDate(rental.getRentDate().plusDays(rental.getDays()));
            if(customer.getIsMember()) rental.setCharge(rental.chargeMemberCal(days));
            else rental.setCharge(rental.chargeCal(days));
            return rentalRepo.save(rental);
        }
    }
    public RentalEntity returnBook(String rentalId){
        RentalEntity rental = getRentalById(rentalId);
        if (rental.getIsReturned()){
            return null;
        }else {
            BookEntity book = getBookById(rental.getBookId());
            book.setIsAvailable(true);
            bookRepo.save(book);

            rental.setIsReturned(true);
            rental.setReturnDate(LocalDateTime.now());
            if(rental.getDueDate().isBefore(rental.getReturnDate())) {
                rental.setIsReturnedLate(true);
                rental.setFines(rental.finesCal((int) ChronoUnit.DAYS.between(rental.getDueDate(), rental.getReturnDate())));
            }
            return rentalRepo.save(rental);
        }
    }
    public void deleteRental(String rentalId) {rentalRepo.deleteById(rentalId);}
}
