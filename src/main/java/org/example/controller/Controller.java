package org.example.controller;

import org.example.es.BookEntity;
import org.example.es.CustomerEntity;
import org.example.es.RentalEntity;
import org.example.service.LibraryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class Controller {
    @Autowired
    private LibraryService libraryService;

    @GetMapping("/getAllBook")
    public List<BookEntity> getAllBook() {return libraryService.getAllBook();}

    @GetMapping("/getBookById")
    public BookEntity getBookById(@RequestParam("bookId") String bookId) {return libraryService.getBookById(bookId);}

    @GetMapping("/getBookByTitle")
    public List<BookEntity> getBookByTitle(@RequestParam("title") String title) {return libraryService.getBookByTitle(title);}

    @GetMapping("/getBookByAuthor")
    public List<BookEntity> getBookByAuthor(@RequestParam("author") String author) {return libraryService.getBookByAuthor(author);}

    @GetMapping("/getBookByIsAvailable")
    public List<BookEntity> getBookByIsAvailable(@RequestParam("isAvailable") boolean isAvailable) {return libraryService.getBookByIsAvailable(isAvailable);}

    @PutMapping("/addBook")
    public BookEntity addBook(@RequestParam("title") String title, @RequestParam("author") String author, @RequestParam("price") double price) {
        return libraryService.addBook(title,author,price);
    }

    @DeleteMapping("/deleteBook")
    public void deleteBook(@RequestParam String id){
        libraryService.deleteBook(id);}

    //

    @GetMapping("/getAllCustomer")
    public List<CustomerEntity> getAllCustomer(){return libraryService.getAllCustomer();}

    @GetMapping("/getCustomerById")
    public CustomerEntity getCustomerById(@RequestParam("customerId") String customerId) {return libraryService.getCustomerById(customerId);}

    @GetMapping("/getCustomerByFirstName")
    public List<CustomerEntity> getCustomerByFirstName(@RequestParam("firstName") String firstName) {return libraryService.getCustomerByFirstName(firstName);}

    @GetMapping("/getCustomerByLastName")
    public List<CustomerEntity> getCustomerByLastName(@RequestParam("lastName") String lastName) {return libraryService.getCustomerByLastName(lastName);}

    @GetMapping("/getCustomerByIsMember")
    public List<CustomerEntity> getCustomerByIsMember(@RequestParam("isMember") boolean isMember) {return libraryService.getCustomerByIsMember(isMember);}

    @PutMapping("/addCustomer")
    public CustomerEntity addCustomer(@RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName, @RequestParam("tel") String tel, @RequestParam("isMember") boolean isMember){
        return libraryService.addCustomer(firstName,lastName,tel,isMember);
    }

    @DeleteMapping("/deleteCustomer")
    public void deleteCustomer(@RequestParam String customerId) {
        libraryService.deleteCustomer(customerId);}

    //

    @GetMapping("/getAllRental")
    public List<RentalEntity> getAllRental() {return libraryService.getAllRental();}

    @GetMapping("/getRentalById")
    public RentalEntity getRentalById(@RequestParam("rentalId") String rentalId) {return libraryService.getRentalById(rentalId);}

    @GetMapping("/getRentalByBookId")
    public List<RentalEntity> getRentalByBookId(@RequestParam("bookId") String bookId) {return libraryService.getRentalByBookId(bookId);}

    @GetMapping("/getRentalByCustomerId")
    public List<RentalEntity> getRentalByCustomerId(@RequestParam("customerId") String customerId) {return libraryService.getRentalByCustomerId(customerId);}

    @GetMapping("/getRentalByIsReturned")
    public List<RentalEntity> getRentalByIsReturned(@RequestParam("isReturned") boolean isReturned) {return libraryService.getRentalByIsReturned(isReturned);}

    @GetMapping("/getRentalByIsReturnedLate")
    public List<RentalEntity> getRentalByReturnedLate(@RequestParam("isReturnedLate") boolean isReturnedLate) {return libraryService.getRentalByIsReturnedLate(isReturnedLate);}

    @PostMapping("/addRental")
    public RentalEntity addRental(@RequestParam("bookId") String bookId, @RequestParam("customerId") String customerId, @RequestParam("days") int days) {
        return libraryService.addRental(bookId,customerId,days);
    }

    @PostMapping("/returnBook")
    public RentalEntity returnBook(@RequestParam("rentalId") String rentalId){return libraryService.returnBook(rentalId);}

    @DeleteMapping("/deleteRental")
    public void deleteRental(@RequestParam String rentalId) {
        libraryService.deleteRental(rentalId);}
}