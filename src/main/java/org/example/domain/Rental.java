package org.example.domain;

import java.time.LocalDateTime;

public class Rental {
    private String rentalId;
    private String bookId;
    private String customerId;
    private int days;
    private boolean isReturned = false;
    private boolean isReturnedLate = false;
    private LocalDateTime rentDate;
    private LocalDateTime dueDate;
    private LocalDateTime returnDate;
    private double charge;
    private double fines;

    public String getRentalId() { return rentalId; }
    public void setRentalId(String rentalId) { this.rentalId = rentalId; }

    public String getBookId() { return bookId; }
    public void setBookId(String bookId) { this.bookId = bookId; }

    public String getCustomerId() { return customerId; }
    public void setCustomerId(String customerId) { this.customerId = customerId; }

    public int getDays() { return days; }
    public void setDays(int days) { this.days = days; }

    public boolean getIsReturned() { return isReturned; }
    public void setIsReturned(boolean isReturned) { this.isReturned = isReturned; }

    public boolean getIsReturnedLate() { return isReturnedLate; }
    public void setIsReturnedLate(boolean isReturnedLate) { this.isReturnedLate = isReturnedLate; }

    public LocalDateTime getRentDate() { return rentDate; }
    public void setRentDate(LocalDateTime rentDate) { this.rentDate = rentDate; }

    public LocalDateTime getDueDate() { return dueDate; }
    public void setDueDate(LocalDateTime dueDate) { this.dueDate = dueDate; }

    public LocalDateTime getReturnDate() { return returnDate; }
    public void setReturnDate(LocalDateTime returnDate) { this.returnDate = returnDate; }

    public double getCharge() { return charge; }
    public void setCharge(double charge) { this.charge = charge; }

    public double getFines() { return fines; }
    public void setFines(double fines) { this.fines = fines; }
}
