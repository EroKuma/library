package org.example.domain;

public class Customer {
    private String customerId;
    private String firstName;
    private String lastName;
    private String tel;
    private boolean isMember;

    public String getCustomerId() { return customerId; }
    public void setCustomerId(String customerId) { this.customerId = customerId; }

    public String getFirstName() { return firstName; }
    public void setFirstName(String firstName) { this.firstName = firstName; }

    public String getLastName() { return lastName; }
    public void setLastName(String lastName) { this.lastName = lastName; }

    public String getTel() { return tel; }
    public void setTel(String tel) { this.tel = tel; }

    public boolean getIsMember() { return isMember; }
    public void setIsMember(boolean isMember) { this.isMember = isMember; }
}
