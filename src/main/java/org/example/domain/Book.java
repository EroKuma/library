package org.example.domain;

public class Book {
    private String bookId;
    private String title;
    private String author;
    private double price;
    private boolean isAvailable;

    public String getBookId() { return bookId; }
    public void setBookId(String bookId) { this.bookId = bookId; }

    public String getTitle() { return title; }
    public void setTitle(String title) { this.title = title; }

    public String getAuthor() { return author; }
    public void setAuthor(String author) { this.author = author; }

    public double getPrice() { return price; }
    public void setPrice(double price) { this.price = price; }

    public boolean getIsAvailable() {return isAvailable;}
    public void setIsAvailable(boolean isAvailable) {this.isAvailable = isAvailable;}
}
